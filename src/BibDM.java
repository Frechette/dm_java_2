import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      if(liste.size()!=0){
        Integer mini = liste.get(0);
        for (Integer i : liste){
          if (i < mini){
            mini = i;
          }
        }
        return mini;
      }
        return null;
    }



    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
     public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
       for(T t :liste){
         if(t.compareTo(valeur) <0){
           return false;
         }
       }
         return true;
     }


    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
     public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
       List<T>liste = new ArrayList<>(liste1);
       for (T t: liste2){
         if(liste.contains(t) == false){
           liste.add(t);
         }
       }
         return liste;
     }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
     public static List<String> decoupe(String texte){
       String[] liste = texte.split(" ");
       List <String> ListeCh = new ArrayList<>();
       for(String str : liste){
         ListeCh.add(str);
       }
       return ListeCh;
     }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

     public static String motMajoritaire(String texte){
       Integer max = null;
       String motMaj = null;
       List<String> liste =  new ArrayList<>();
       List<String> res =  decoupe(texte);
       Collections.sort(res);
       for(int i=0; i< res.size(); i+=max){
         if(max == null || max < Collections.frequency(res, res.get(i))){ //frequency() -> compte la fréquence d'un elt dans une liste
           max = Collections.frequency(res, res.get(i));
           motMaj = res.get(i);
         }
       }
       return motMaj;
     }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
     public static boolean bienParenthesee(String chaine){
         int cpt = 0;
         for (int i= 0; i<chaine.length(); ++i){
           char lettre = chaine.charAt(i);
           if (lettre == '('){
             cpt +=1;
           }
           if (lettre == ')'){
             cpt -=1;
           }
           if (cpt == -1){
             return false;
           }
         }
         if (cpt == 0){
           return true;
         }
         return false;
     }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
     public static boolean bienParentheseeCrochets(String chaine){
       int j =-1;
       ArrayList<String> liste = new ArrayList<>();
       for (int i= 0; i<chaine.length(); ++i){
         char lettre = chaine.charAt(i);
         if (lettre == '('){
           liste.add("(");
           j+=1;
         }
         if (lettre == ')'){
           if (!(liste.isEmpty()) && liste.get(j) =="("){
             liste.remove(j);
             j-=1;
           }
           else{
             return false;
           }
         }
         if (lettre == '['){
           liste.add("[");
           j+=1;
         }
         if (lettre == ']'){
           if (!(liste.isEmpty()) && liste.get(j) =="]"){
             liste.remove(j);
             j-=1;
           }
           else{
             return false;
           }
         }
       }
       if (!(liste.isEmpty())){
         return false;
       }
       return true;
     }


    /**
     * Recherche par dichotomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      Integer debut = 0;
      Integer fin = liste.size()-1;
      Integer milieu;
      while(debut < fin)
      {
        milieu = (debut + fin)/2;
        if (liste.get(milieu)<valeur)
        {
          debut += 1;
        }
        else
        {
          fin = milieu;
        }
      }
      return (debut < liste.size() && valeur.equals(liste.get(debut)));
      }
}
